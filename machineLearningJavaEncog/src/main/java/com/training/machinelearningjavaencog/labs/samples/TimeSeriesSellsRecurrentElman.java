/*
 * Queda pendeinte el entrenamiento ya que no es sencilla la configuración para una convergencia
 */
package com.training.machinelearningjavaencog.labs.samples;


import com.training.machinelearningjavaencog.neusysops.data.file.FileInteraction;
import com.training.machinelearningjavaencog.neusysops.data.management.DataCasting;
import com.training.machinelearningjavaencog.neusysops.data.management.DataScaling;
import com.training.machinelearningjavaencog.neusysops.data.management.DataStructuring;
import com.training.machinelearningjavaencog.neusysops.data.management.DataUtils;
import com.training.machinelearningjavaencog.neusysops.network.ElmanRNN;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author rorodriguez
 */
public class TimeSeriesSellsRecurrentElman {
    public static final boolean WITH_BIAS = true;
    public static final boolean WITHOUT_BIAS = false;
    //public static final String TRAININGFILENAME = "/dataAnalisis/tous/DataEntrenam-NormFT7Ent-1Sal-005RecurElman/Entrenamiento.csv";
    //public static final String TESTFILENAME = "/dataAnalisis/tous/DataEntrenam-NormFT7Ent-1Sal-005RecurElman/Prueba.csv";
    public static final String TRAININGFILENAME = "E:\\rrodriguez\\Proyectos\\neusys\\neusys\\src\\main\\resources\\DataAnalisis\\tous\\DataEntrenam-NormFT7Ent-1Sal-005RecurElman\\Entrenamiento.csv";
    public static final String TESTFILENAME = "E:\\rrodriguez\\Proyectos\\neusys\\neusys\\src\\main\\resources\\DataAnalisis\\tous\\DataEntrenam-NormFT7Ent-1Sal-005RecurElman\\Prueba.csv";
    public static final String ENCOGFILENAME = "E:\\rrodriguez\\Proyectos\\neusys\\neusys\\src\\main\\resources\\DataAnalisis\\tous\\DataEntrenam-NormFT7Ent-1Sal-005RecurElman\\encognetwork005_14Helman2.eg";
    public static final String RESULTFILENAME = "E:\\rrodriguez\\Proyectos\\neusys\\neusys\\src\\main\\resources\\DataAnalisis\\tous\\DataEntrenam-NormFT7Ent-1Sal-005RecurElman\\Resultados005_14Helman2.csv";
    
    public static void main(String[] args) throws IOException{
        TimeSeriesSellsRecurrentElman tssre = new  TimeSeriesSellsRecurrentElman();
        tssre.run();
        
    }
    
    public void run() throws IOException{
        double[][] data = extractData(TRAININGFILENAME,8,1);
        double[][] input = DataUtils.getColumnsFromMatrixIntoMatrix(data, 0,7);
        double[][] ideal = DataUtils.getColumnsFromMatrixIntoMatrix(data, 7, 8);
        double[][] testData = extractData(TESTFILENAME,8,1);
        double[][] testInput =DataUtils.getColumnsFromMatrixIntoMatrix(testData, 0,7);
        double[][] testIdeal =DataUtils.getColumnsFromMatrixIntoMatrix(testData, 7,8);
        ElmanRNN ernn = new ElmanRNN();
        ernn.setSigmoideanActivationToNetwork();
        ernn.setInputNeurons(7);
        ernn.setHiddenNeurons(14);
        ernn.setOutputNeurons(1);
        ernn.generatePatternIntoNeuralNetwork();
        ernn.setTrainingDataSet(input, ideal);
        ernn.setTestingDataSet(testInput, testIdeal);
        LocalDateTime startDate = LocalDateTime.now();
        ernn.trainNetwork(0.005);
        LocalDateTime finishDate = LocalDateTime.now();
        System.out.println("Training Finished!");
        System.out.println("Training started on: " +startDate);
        System.out.println("Training finished on: "+finishDate);
        Duration duration = Duration.between(startDate, finishDate);
        System.out.println("Training Time in Minutes: "+ duration.toMinutes());
        System.out.println("NeuralNetworkResults:");
        List<List<Double>> results= ernn.testNetwork();
        FileInteraction.writeDoubleObservationsMatrixToCSV(RESULTFILENAME, results);
        ernn.shutDownEngine();
        ernn.persistNetwork(ENCOGFILENAME);
    }
    
    private static double[][] extractData(String filePath,int window, int lag) throws IOException{
         List<String> observaciones = FileInteraction.extractObservationsFromTxtFile(filePath);
         double[] castedObs = DataCasting.castFromStringListToDoubleArray(observaciones);
         //double[] scaledObs = DataScaling.arrayFeatureScaling(castedObs);
         double[][] structuredObs = DataStructuring.toNColumnArrayStructureWithLag(castedObs, window,lag);
         return structuredObs;
    }
}
