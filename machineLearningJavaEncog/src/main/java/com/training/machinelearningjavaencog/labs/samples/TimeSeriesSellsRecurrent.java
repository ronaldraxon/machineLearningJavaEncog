/*
 * Queda pendeinte el entrenamiento ya que no es sencilla la configuración para una convergencia
 */
package com.training.machinelearningjavaencog.labs.samples;

import com.training.machinelearningjavaencog.neusysops.data.file.FileInteraction;
import com.training.machinelearningjavaencog.neusysops.data.management.DataCasting;
import com.training.machinelearningjavaencog.neusysops.data.management.DataScaling;
import com.training.machinelearningjavaencog.neusysops.data.management.DataStructuring;
import com.training.machinelearningjavaencog.neusysops.data.management.DataUtils;
import com.training.machinelearningjavaencog.neusysops.network.RNN;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author rorodriguez
 */
public class TimeSeriesSellsRecurrent {
        public static final boolean WITH_BIAS = true;
        public static final boolean WITHOUT_BIAS = false;
        public static final String TRAININGFILENAME = "/dataAnalisis/tous/DataEntrenam-NormFT60Ent-30Sal - 0063 RECUR Smoothing/Entrenamiento.csv";
        public static final String TESTFILENAME = "/dataAnalisis/tous/DataEntrenam-NormFT60Ent-30Sal - 0063 RECUR Smoothing/Prueba.csv";
        public static final String ENCOGFILENAME = "/dataAnalisis/tous/DataEntrenam-NormFT60Ent-30Sal - 0063 RECUR Smoothing/encognetwork.eg";
        public static final String RESULTFILENAME = "/dataAnalisis/tous/DataEntrenam-NormFT60Ent-30Sal - 0063 RECUR Smoothing/Resultados.csv";
        public static void main(String[] args) throws IOException{
        TimeSeriesSellsRecurrent tssr = new TimeSeriesSellsRecurrent();
        tssr.run();
    }
    
    public void run() throws IOException{
        double[][] data = extractData(TRAININGFILENAME,90,1);
        double[][] input = DataUtils.getColumnsFromMatrixIntoMatrix(data, 0,60);
        double[][] ideal = DataUtils.getColumnsFromMatrixIntoMatrix(data, 60, 90);
        double[][] testData = extractData(TESTFILENAME,90,1);
        double[][] testInput =DataUtils.getColumnsFromMatrixIntoMatrix(testData, 0,60);
        double[][] testIdeal =DataUtils.getColumnsFromMatrixIntoMatrix(testData, 60,90);
        RNN rnn = new RNN();
        rnn.createDefaultTopology();
        //rnn.finalizeConstruction();
        rnn.setTrainingDataSet(input, ideal);
        rnn.setTestingDataSet(testInput, testIdeal);
        LocalDateTime startDate = LocalDateTime.now();
        //rnn.trainNetworkWithBackPropagation(0.005,0.01,0.7);
        rnn.trainNetworkWithBackPropagation(0.006);
        //rnn.trainNetworkWithLevenbergMarquart(0.005);
        
        LocalDateTime finishDate = LocalDateTime.now();
        System.out.println("Training Finished!");
        System.out.println("Training started on: " +startDate);
        System.out.println("Training finished on: "+finishDate);
        Duration duration = Duration.between(startDate, finishDate);
        System.out.println("Training Time in Minutes: "+ duration.toMinutes());
        System.out.println("NeuralNetworkResults:");
        List<List<Double>> results= rnn.testNetwork();
        FileInteraction.writeDoubleObservationsMatrixToCSV(RESULTFILENAME, results);
        rnn.shutDownEngine();
        rnn.persistNetwork(ENCOGFILENAME);
        
    }
    private static double[][] extractData(String filePath,int window, int lag) throws IOException{
         List<String> observaciones = FileInteraction.extractObservationsFromTxtFile(filePath);
         double[] castedObs = DataCasting.castFromStringListToDoubleArray(observaciones);
         double[] scaledObs = DataScaling.arrayFeatureScaling(castedObs);
         double[][] structuredObs = DataStructuring.toNColumnArrayStructureWithLag(scaledObs, window,lag);
         return structuredObs;
    }
}
