/*
 * Queda pendeinte el entrenamiento ya que no es sencilla la configuración para una convergencia
 */
package com.training.machinelearningjavaencog.labs.samples;


import com.training.machinelearningjavaencog.neusysops.data.file.FileInteraction;
import com.training.machinelearningjavaencog.neusysops.data.management.DataCasting;
import com.training.machinelearningjavaencog.neusysops.data.management.DataScaling;
import com.training.machinelearningjavaencog.neusysops.data.management.DataStructuring;
import com.training.machinelearningjavaencog.neusysops.data.management.DataUtils;
import com.training.machinelearningjavaencog.neusysops.network.JordanRNN;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;


/**
 *
 * @author rorodriguez
 */
public class TimeSeriesSellsRecurrentJordan {
    public static final boolean WITH_BIAS = true;
    public static final boolean WITHOUT_BIAS = false;
    public static final String TRAININGFILENAME = "E:\\rrodriguez\\Proyectos\\neusys\\neusys\\src\\main\\resources\\DataAnalisis\\cash4u\\DataEntrenam-NormFT5Ent-1Sal-005_Jordan\\Entrenamiento.csv";
    public static final String TESTFILENAME = "E:\\rrodriguez\\Proyectos\\neusys\\neusys\\src\\main\\resources\\DataAnalisis\\cash4u\\DataEntrenam-NormFT5Ent-1Sal-005_Jordan\\Prueba.csv";
    public static final String ENCOGFILENAME = "E:\\rrodriguez\\Proyectos\\neusys\\neusys\\src\\main\\resources\\DataAnalisis\\cash4u\\DataEntrenam-NormFT5Ent-1Sal-005_Jordan\\encognetwork005_18HJordan.eg";
    public static final String RESULTFILENAME = "E:\\rrodriguez\\Proyectos\\neusys\\neusys\\src\\main\\resources\\DataAnalisis\\cash4u\\DataEntrenam-NormFT5Ent-1Sal-005_Jordan\\Resultados005_18HJordan.csv";
    
    public static void main(String[] args) throws IOException{
        TimeSeriesSellsRecurrentJordan tssRJ = new TimeSeriesSellsRecurrentJordan();
        tssRJ.run();
    }
    
    public void run() throws IOException{
        double[][] data = extractData(TRAININGFILENAME,6,1);
        double[][] input = DataUtils.getColumnsFromMatrixIntoMatrix(data, 0,5);
        double[][] ideal = DataUtils.getColumnsFromMatrixIntoMatrix(data, 5, 6);
        double[][] testData = extractData(TESTFILENAME,6,1);
        double[][] testInput =DataUtils.getColumnsFromMatrixIntoMatrix(testData, 0,5);
        double[][] testIdeal =DataUtils.getColumnsFromMatrixIntoMatrix(testData, 5,6);
        JordanRNN jrnn = new JordanRNN();
        jrnn.setSigmoideanActivationToNetwork();
        jrnn.setInputNeurons(5);
        jrnn.setHiddenNeurons(15);
        jrnn.setOutputNeurons(1);
        jrnn.generatePatternIntoNeuralNetwork();
        jrnn.setTrainingDataSet(input, ideal);
        jrnn.setTestingDataSet(testInput, testIdeal);
        LocalDateTime startDate = LocalDateTime.now();
        jrnn.trainNetwork(0.0001,0.6,0.005);
        LocalDateTime finishDate = LocalDateTime.now();
        System.out.println("Training Finished!");
        System.out.println("Training started on: " +startDate);
        System.out.println("Training finished on: "+finishDate);
        Duration duration = Duration.between(startDate, finishDate);
        System.out.println("Training Time in Minutes: "+ duration.toMinutes());
        System.out.println("NeuralNetworkResults:");
        List<List<Double>> results= jrnn.testNetwork();
        FileInteraction.writeDoubleObservationsMatrixToCSV(RESULTFILENAME, results);
        jrnn.shutDownEngine();
        jrnn.persistNetwork(ENCOGFILENAME);
    }
    
    private static double[][] extractData(String filePath,int window, int lag) throws IOException{
         List<String> observaciones = FileInteraction.extractObservationsFromTxtFile(filePath);
         double[] castedObs = DataCasting.castFromStringListToDoubleArray(observaciones);
         //double[] scaledObs = DataScaling.arrayFeatureScaling(castedObs);
         double[][] structuredObs = DataStructuring.toNColumnArrayStructureWithLag(castedObs, window,lag);
         return structuredObs;
    }
}
