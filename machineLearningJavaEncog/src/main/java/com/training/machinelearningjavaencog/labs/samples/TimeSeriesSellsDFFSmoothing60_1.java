/* Experimento 
 * DataEntrenam-NormFT60Ent-30Sal
 * 
 */
package com.training.machinelearningjavaencog.labs.samples;

import com.training.machinelearningjavaencog.neusysops.data.file.FileInteraction;
import com.training.machinelearningjavaencog.neusysops.data.management.DataCasting;
import com.training.machinelearningjavaencog.neusysops.data.management.DataScaling;
import com.training.machinelearningjavaencog.neusysops.data.management.DataStructuring;
import com.training.machinelearningjavaencog.neusysops.data.management.DataUtils;
import com.training.machinelearningjavaencog.neusysops.network.DFF;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;


/**
 *
 * @author raxonserver
 */
public class TimeSeriesSellsDFFSmoothing60_1 {
    public static final boolean WITH_BIAS = true;
    public static final boolean WITHOUT_BIAS = false;
    //public static final String TRAININGFILENAME = "/dataAnalisis/tous/DataEntrenam-NormFT60Ent-01Sal-005DFFSmoothing/Entrenamiento.csv";
    //public static final String TESTFILENAME = "/dataAnalisis/tous/DataEntrenam-NormFT60Ent-01Sal-005DFFSmoothing/Prueba.csv";
    public static final String TRAININGFILENAME = "D:\\Shared_raxonserver\\workAndStudy\\officeProjects\\dataAnalisis\\neusys\\neusys\\src\\main\\resources\\dataAnalisis\\tous\\DataEntrenam-NormFT60Ent-01Sal-001DFFSmoothing\\Entrenamiento2013.csv";
    public static final String TESTFILENAME = "D:\\Shared_raxonserver\\workAndStudy\\officeProjects\\dataAnalisis\\neusys\\neusys\\src\\main\\resources\\dataAnalisis\\tous\\DataEntrenam-NormFT60Ent-01Sal-001DFFSmoothing\\Prueba2013.csv";
    public static final String ENCOGFILENAME = "D:\\Shared_raxonserver\\workAndStudy\\officeProjects\\dataAnalisis\\neusys\\neusys\\src\\main\\resources\\dataAnalisis\\tous\\DataEntrenam-NormFT60Ent-01Sal-001DFFSmoothing\\encognetwork001DFSmooth2013.eg";
    public static final String RESULTFILENAME = "D:\\Shared_raxonserver\\workAndStudy\\officeProjects\\dataAnalisis\\neusys\\neusys\\src\\main\\resources\\dataAnalisis\\tous\\DataEntrenam-NormFT60Ent-01Sal-001DFFSmoothing\\Resultados001DFSmooth2013.csv";
    public static void main(String[] args) throws IOException{
       TimeSeriesSellsDFFSmoothing60_1 tss = new TimeSeriesSellsDFFSmoothing60_1();
       tss.run();
    }
    private void run() throws IOException{
        double[][] data = extractData(TRAININGFILENAME,61,1);
        double[][] input = DataUtils.getColumnsFromMatrixIntoMatrix(data, 0,60);
        double[][] ideal = DataUtils.getColumnsFromMatrixIntoMatrix(data, 60, 61);
        double[][] testData = extractData(TESTFILENAME,61,1);
        double[][] testInput =DataUtils.getColumnsFromMatrixIntoMatrix(testData, 0,60);
        double[][] testIdeal =DataUtils.getColumnsFromMatrixIntoMatrix(testData, 60,61);
        DFF dff = new DFF();
        dff.addBasicLayerToNetwork(WITH_BIAS,60);
        dff.addBasicSigmoideanLayerToNetwork(WITH_BIAS, 120);
        dff.addBasicSigmoideanLayerToNetwork(WITH_BIAS, 180);
        dff.addBasicSigmoideanLayerToNetwork(WITH_BIAS, 360);
        dff.addBasicSigmoideanLayerToNetwork(WITH_BIAS, 180);
        dff.addBasicSigmoideanLayerToNetwork(WITH_BIAS, 120);
        dff.addBasicSigmoideanLayerToNetwork(WITHOUT_BIAS, 1);
        dff.finalizeDFFConstruction();
        dff.setTrainingDataSet(input, ideal);
        dff.setTestingDataSet(testInput, testIdeal);
        LocalDateTime startDate = LocalDateTime.now();
        dff.trainNetworkWithBackPropagation(0.001);
        LocalDateTime finishDate = LocalDateTime.now();
        System.out.println("Training Finished!");
        System.out.println("Training started on: " +startDate);
        System.out.println("Training finished on: "+finishDate);
        Duration duration = Duration.between(startDate, finishDate);
        System.out.println("Training Time in Minutes: "+ duration.toMinutes());
        System.out.println("NeuralNetworkResults:");
        List<List<Double>> results= dff.testNetwork();
        FileInteraction.writeDoubleObservationsMatrixToCSV(RESULTFILENAME, results);
        dff.shutDownEngine();
        dff.persistNetwork(ENCOGFILENAME);
    }
    
    private static double[][] extractData(String filePath,int window, int lag) throws IOException{
         //List<String> observaciones = FileInteraction.extractObservationsFromTxtResource(filePath);
         List<String> observaciones = FileInteraction.extractObservationsFromTxtFile(filePath);
         double[] castedObs = DataCasting.castFromStringListToDoubleArray(observaciones);
         //double[] scaledObs = DataScaling.arrayFeatureScaling(castedObs);
         double[][] structuredObs = DataStructuring.toNColumnArrayStructureWithLag(castedObs, window,lag);
         return structuredObs;
    }
}
