/*
 *Queda pendiente el experimento por falta de información acerca de como normalizar
 */
package com.labs.tous;


import com.training.machinelearningjavaencog.neusysops.data.file.FileInteraction;
import com.training.machinelearningjavaencog.neusysops.data.management.DataCasting;
import com.training.machinelearningjavaencog.neusysops.data.management.DataScaling;
import com.training.machinelearningjavaencog.neusysops.data.management.DataStructuring;
import com.training.machinelearningjavaencog.neusysops.data.management.DataUtils;
import com.training.machinelearningjavaencog.neusysops.network.NEAT;
import java.io.IOException;
import java.util.List;
/**
 *
 * @author rorodriguez
 */
public class TimeSeriesSellsNeatPopulation {
    public static double XOR_INPUT[][] = { { 0.0, 0.0 }, { 1.0, 0.0 },
                                           { 0.0, 1.0 }, { 1.0, 1.0 } };
    public static double XOR_IDEAL[][] = { { 0.0 }, { 1.0 }, { 1.0 }, { 0.0 } };
    public static final String TRAININGFILENAME = "/dataAnalisis/tous/DataEntrenam-NormFT60Ent-30Sal-005NEATSmoothing/Entrenamiento.csv";
    public static final String TESTFILENAME = "/dataAnalisis/tous/DataEntrenam-NormFT60Ent-30Sal-005NEATSmoothing/Prueba.csv";
    public static final String ENCOGFILENAME = "D:\\Shared_raxonserver\\workAndStudy\\officeProjects\\dataAnalisis\\neusys\\neusys\\src\\main\\resources\\dataAnalisis\\tous\\DataEntrenam-NormFT60Ent-30Sal-005NEATSmoothing\\NEATnetwork005.eg";
    public static final String RESULTFILENAME = "D:\\Shared_raxonserver\\workAndStudy\\officeProjects\\dataAnalisis\\neusys\\neusys\\src\\main\\resources\\dataAnalisis\\tous\\DataEntrenam-NormFT60Ent-30Sal-005NEATSmoothing\\NEATResultados005.csv";
    
    public static void main(String args[]) throws IOException {
        TimeSeriesSellsNeatPopulation tssNP = new TimeSeriesSellsNeatPopulation();
        tssNP.run();
    }
    
    public static void run() throws IOException {
        double[][] data = extractData1(TRAININGFILENAME,90,1);
        double[][] input = DataUtils.getColumnsFromMatrixIntoMatrix(data, 0,60);
        double[][] ideal = DataUtils.getColumnsFromMatrixIntoMatrix(data, 60, 90);
        double[][] testData = extractData1(TESTFILENAME,90,1);
        double[][] testInput =DataUtils.getColumnsFromMatrixIntoMatrix(testData, 0,60);
        double[][] testIdeal =DataUtils.getColumnsFromMatrixIntoMatrix(testData, 60,90);
        NEAT neat = new NEAT(60,30,5);
        //NEAT neat = new NEAT(2,1,1000);
        //neat.setTrainingDataSet(XOR_INPUT, XOR_IDEAL);
        neat.setTrainingDataSet(input, ideal);
        neat.setTestingDataSet(testInput, testIdeal);
        neat.setInitialConnectionDensity(1.0);
        neat.resetNetworkPopulation();
        neat.calculateTrainingScore();
        //neat.setActivationStep();
        neat.constructNetworkTrainerWithNEATUtil();
        //neat.trainPopulationWithEncogUtility();
        neat.trainPopulationWithEvolutionaryAlgorithm(0.05);
        neat.extractBestSpecimenFromPopulation();
        List<List<Double>> results = neat.testNetwork();
        FileInteraction.writeDoubleObservationsMatrixToCSV(RESULTFILENAME, results);
        //neat.testNetworkWithEncogUtility();
        neat.shutDownEngine();
        //neat.persistNetwork(ENCOGFILENAME);
    }
        
    private static double[][] extractData(String filePath,int window, int lag) throws IOException{
         List<String> observaciones = FileInteraction.extractObservationsFromTxtFile(filePath);
         double[] castedObs = DataCasting.castFromStringListToDoubleArray(observaciones);
         double[] scaledObs = DataScaling.arrayFeatureScaling(castedObs);
         double[][] structuredObs = DataStructuring.toNColumnArrayStructureWithLag(scaledObs, window,lag);
         return structuredObs;
    }
    
    private static double[][] extractData1(String filePath,int window, int lag) throws IOException{
         List<String> observaciones = FileInteraction.extractObservationsFromTxtResource(filePath);
         double[] castedObs = DataCasting.castFromStringListToDoubleArray(observaciones);
         double[][] structuredObs = DataStructuring.toNColumnArrayStructureWithLag(castedObs, window,lag);
         return structuredObs;
    }
}
