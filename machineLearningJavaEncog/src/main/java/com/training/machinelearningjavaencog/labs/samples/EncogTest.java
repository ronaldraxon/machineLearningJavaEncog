package com.neusysops.encogtest;


import com.training.machinelearningjavaencog.neusysops.data.file.FileInteraction;
import com.training.machinelearningjavaencog.neusysops.data.management.DataCasting;
import com.training.machinelearningjavaencog.neusysops.data.management.DataScaling;
import com.training.machinelearningjavaencog.neusysops.data.management.DataStructuring;
import com.training.machinelearningjavaencog.neusysops.data.management.DataUtils;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.RPROPType;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.persist.EncogDirectoryPersistence;

/**
*
*@authorraxonserver
*/
public class EncogTest{
public static final String ENCOGFILENAME = "C:\\Users\\raxonserver\\Documents\\NetBeansProjects\\neusys\\src\\main\\resources\\encogexample.eg";
    
public static void main(final String args[]) throws IOException{

BasicNetwork network = new BasicNetwork();
network.addLayer(new BasicLayer(null,true,60));
network.addLayer(new BasicLayer(new ActivationSigmoid(),true,120));
network.addLayer(new BasicLayer(new ActivationSigmoid(),true,180));
network.addLayer(new BasicLayer(new ActivationSigmoid(),true,360));
network.addLayer(new BasicLayer(new ActivationSigmoid(),true,180));
network.addLayer(new BasicLayer(new ActivationSigmoid(),true,120));
network.addLayer(new BasicLayer(new ActivationSigmoid(),true,60));
network.addLayer(new BasicLayer(new ActivationSigmoid(),false,30));
network.getStructure().finalizeStructure();
network.reset();
double[][] datos = extraerDatos("C:\\Users\\raxonserver\\Documents\\NetBeansProjects\\neusys\\src\\main\\resources\\datafiles\\tous\\ImportePorFecha.csv");
double[][] input = DataUtils.getColumnsFromMatrixIntoMatrix(datos, 0,60);
double[][] ideal = DataUtils.getColumnsFromMatrixIntoMatrix(datos, 60, 90);
double[][] datosTest = extraerDatos("C:\\Users\\raxonserver\\Documents\\NetBeansProjects\\neusys\\src\\main\\resources\\datafiles\\tous\\TestImportePorFecha.csv");
double[][] testInput =DataUtils.getColumnsFromMatrixIntoMatrix(datosTest, 0,60);
double[][] testIdeal =DataUtils.getColumnsFromMatrixIntoMatrix(datosTest, 60,90);
MLDataSet trainingDataSet= new BasicMLDataSet(input,ideal);
MLDataSet testingDataSet= new BasicMLDataSet(testInput, testIdeal);
//final ResilientPropagation train = new ResilientPropagation(network,trainingDataSet);
//train.setRPROPType(RPROPType.iRPROPp);
//final Backpropagation train = new Backpropagation(network,trainingDataSet,0.1,0.9);
final Backpropagation train = new Backpropagation(network,trainingDataSet);
LocalDateTime startDate = LocalDateTime.now();
int epoch=1;
    do{
        train.iteration();
        System.out.println("Epoch# "+epoch+" - Error: "+train.getError());epoch++;
       }while(train.getError()>0.006);
        train.finishTraining();
        LocalDateTime finishDate = LocalDateTime.now();
        System.out.println("Training Finished!");
        System.out.println("Training started on: " +startDate);
        System.out.println("Training finished on: "+finishDate);
        Duration duration = Duration.between(startDate, finishDate);
        System.out.println("Training Time in Minutes: "+ duration.toMinutes());
        System.out.println("NeuralNetworkResults:");
        
        for(MLDataPair pair : testingDataSet){
            final MLData output=network.compute(pair.getInput());
            for(int i=0; i<output.size();i++){
                System.out.println("actual= "+output.getData(i)+",ideal="+pair.getIdeal().getData(i));
            }
            
        }
        Encog.getInstance().shutdown();
        EncogDirectoryPersistence.saveObject(new File(ENCOGFILENAME), network);
}

    private static double[][] extraerDatos(String rutaArchivo) throws IOException{
         List<String> observaciones = FileInteraction.extractObservationsFromTxtFile(rutaArchivo);
         double[] castedObs = DataCasting.castFromStringListToDoubleArray(observaciones);
         double[] scaledObs = DataScaling.arrayFeatureScaling(castedObs);
         double[][] structuredObs = DataStructuring.toNColumnArrayStructureWithLag(scaledObs, 90,1);
         return structuredObs;
    }
    
    private static double[][] extraerDatos2(String rutaArchivo) throws IOException{
         List<List<String>> observaciones = FileInteraction.extractObservationsFromCSVFile(rutaArchivo);
         double[][] castedObs = DataCasting.castFromStringListToDoubleMatrix(observaciones);
         double[][] minMaxOfFeatures = DataUtils.getMinimumAndMaximumOfAllFeatures(castedObs);
         double[][] scaledObs = DataScaling.matrixFeatureScaling(castedObs, minMaxOfFeatures);
         return scaledObs;
    }
    
    private static double[][] extraerDatos3(String rutaArchivo) throws IOException{
         List<String> observaciones = FileInteraction.extractObservationsFromTxtFile(rutaArchivo);
         double[] castedObs = DataCasting.castFromStringListToDoubleArray(observaciones);
         double[] scaledObs = DataScaling.arrayFeatureScaling(castedObs);
         double[][] structuredObs = DataStructuring.toNColumnArrayStructureWithLag(scaledObs, 90,1);
         return structuredObs;
    }
    
    public BasicNetwork loadNetworkFromEGFile(String egFilePath)
    {
        System.out.println("Loading network");
        return (BasicNetwork) EncogDirectoryPersistence.loadObject(new File (egFilePath)) ;
    }
    
}
