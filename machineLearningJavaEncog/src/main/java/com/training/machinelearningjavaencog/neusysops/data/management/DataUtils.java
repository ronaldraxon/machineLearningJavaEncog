package com.training.machinelearningjavaencog.neusysops.data.management;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author rorodriguez
 */
public class DataUtils {
    
    public static List<Double> getColumnFromMatrixIntoList(double[][] matrix,int column){
        List<Double> doubleList = new ArrayList<>();
        for(double[] row : matrix){
            doubleList.add(row[column]);
        }
        return doubleList;
    }
    
    public static double[] getColumnFromMatrixIntoArray(double[][] matrix,int column){
        double[] doubleArray = new double[matrix.length];
        int i =0;
        for(double[] row : matrix){
            doubleArray[i] = row[column];
            i++;
        }
        return doubleArray;
    }
    
    public static double[][] getColumnFromMatrixIntoMatrix(double[][] matrix,int column){
        double[][] doubleArray = new double[matrix.length][1];
        int i =0;
        for(double[] row : matrix){
            doubleArray[i][0] = row[column];
            i++;
        }
        return doubleArray;
    }
    
    public static double[][] getColumnsFromMatrixIntoMatrix(double[][] matrix,int startColumn,int endColumn){
        double[][] doubleArray = new double[matrix.length][endColumn-startColumn];
        int arrayRow=0;
        for(double[] observation : matrix){
            int arrayColumn=0;
            for(int observationColumn = startColumn; observationColumn<endColumn;observationColumn++){
                doubleArray[arrayRow][arrayColumn] = observation[observationColumn];
                arrayColumn++;
            }
            arrayRow++;
        }
        return doubleArray;
    }
    
    public static double[][] removeColumnFromMatrixIntoArray(double[][] matrix,int column){
        double[][] doubleArray = new double[matrix.length][matrix[0].length-1];
        int k =0;
        for(int i =0; i<matrix.length;i++){
            k =0;
            for(int j = 0; j <matrix[0].length;j++){
                if(j != column){
                    doubleArray[i][k] = matrix[i][j];
                    k++;
                }
            }
        }
        return doubleArray;
    }
    
    public static double[][] getMinimumAndMaximumOfAllFeatures(double[][] matrix){
        double[][] minimumMaximumArray = new double[2][matrix[0].length];
        for(int i = 0;i<matrix[0].length;i++){
            double[] featureArray = new double[matrix.length];
            int j =0;
            for(double[] row : matrix){
                featureArray[j] = row[i];
                j++;
            }
            minimumMaximumArray[0][i] = DataStatistics.getMinValueFromArray(featureArray);
            minimumMaximumArray[1][i] = DataStatistics.getMaxValueFromArray(featureArray);
        }
        return  minimumMaximumArray;
    }
    
}
