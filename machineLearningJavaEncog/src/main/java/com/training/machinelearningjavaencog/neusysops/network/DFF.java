package com.training.machinelearningjavaencog.neusysops.network;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationLinear;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.engine.network.activation.ActivationTANH;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.lma.LevenbergMarquardtTraining;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.EncogDirectoryPersistence;

/**
 *
 * @author rorodriguez
 */

public class DFF {
    BasicNetwork network;
    MLDataSet trainingDataSet;
    MLDataSet crossDataSet;
    MLDataSet testingDataSet;
    List<List<Double>> testingResults;
    
    public DFF(){
        this.network = new BasicNetwork();
    }
    
    public void finalizeDFFConstruction(){
        network.getStructure().finalizeStructure();
        network.reset();
    }
    
    public void setTrainingDataSet(double[][] input, double[][] ideal){
        trainingDataSet= new BasicMLDataSet(input,ideal);
    }
    
    public void setCrossDataSet(double[][] input, double[][] ideal){
        crossDataSet= new BasicMLDataSet(input,ideal);
    }
    
    public void setTestingDataSet(double[][] input, double[][] ideal){
        testingDataSet= new BasicMLDataSet(input,ideal);
    }
    
    public void addBasicLayerToNetwork(Boolean hasBiasNeuron, Integer numberOfNeurons){
        network.addLayer(new BasicLayer(null,hasBiasNeuron,numberOfNeurons));
    }
    
    public void addBasicLinearLayerToNetwork(Boolean hasBiasNeuron, Integer numberOfNeurons){
        network.addLayer(new BasicLayer(new ActivationLinear(),hasBiasNeuron,numberOfNeurons));
    }
    
    public void addBasicSigmoideanLayerToNetwork(Boolean hasBiasNeuron, Integer numberOfNeurons){
        network.addLayer(new BasicLayer(new ActivationSigmoid(),hasBiasNeuron,numberOfNeurons));
    }
    
    public void addBasicTanHLayerToNetwork(Boolean hasBiasNeuron, Integer numberOfNeurons){
        network.addLayer(new BasicLayer(new ActivationTANH(),hasBiasNeuron,numberOfNeurons));
    }
    
    public void createDefaultTopology(Integer numberOfInputNeurons,Integer numberOfHiddenLayers,Integer numberOfOutputNeurons){
        addBasicLayerToNetwork(true, numberOfInputNeurons);
        for(int i = 0 ; i < numberOfHiddenLayers ; i++){
            addBasicSigmoideanLayerToNetwork(true,network.getLayerNeuronCount(i)*2);
        }
        addBasicSigmoideanLayerToNetwork(false,numberOfOutputNeurons);
        network.getStructure().finalizeStructure();
        network.reset();
    }

    public void trainNetworkWithResilientPropagation(double trainingError){
        ResilientPropagation training = new ResilientPropagation(network,trainingDataSet);
        int epoch=1;
        do{
            training.iteration();
            System.out.println("Epoch#: "+epoch+" - Error: "+training.getError());
            epoch++;
        }while(training.getError()>trainingError);
        training.finishTraining();
    }
    
    public void trainNetworkWithBackPropagation(double trainingError, double learningRate, double momentum){
        Backpropagation bpgTraining = new Backpropagation(network,trainingDataSet,learningRate,momentum);
        int epoch=1;
        do{
            bpgTraining.iteration();
            System.out.println("Epoch#: "+epoch+" - Error: "+bpgTraining.getError());
            epoch++;
        }while(bpgTraining.getError()>trainingError);
        bpgTraining.finishTraining();
    }
    
    public void trainNetworkWithBackPropagation(double trainingError){
        Backpropagation bpgTraining = new Backpropagation(network,trainingDataSet);
        int epoch=1;
        do{
            bpgTraining.iteration();
            System.out.println("Epoch#: "+epoch+" - Error: "+bpgTraining.getError());
            epoch++;
        }while(bpgTraining.getError()>trainingError);
        bpgTraining.finishTraining();
    }
    
    public void trainNetworkWithLevenbergMarquardt(double trainingError){
        LevenbergMarquardtTraining lmaTraining = new LevenbergMarquardtTraining(network,trainingDataSet);
        int epoch=1;
        do{
            lmaTraining.iteration();
            System.out.println("Epoch#: "+epoch+" - Error: "+lmaTraining.getError());
            epoch++;
        }while(lmaTraining.getError()>trainingError);
        lmaTraining.finishTraining();
    }
    
    public List<List<Double>> testNetworkWithCrossValidation(){
        testingResults = new ArrayList<>();
        for(MLDataPair pair : crossDataSet){
            final MLData output = network.compute(pair.getInput());
            for(int i=0; i<output.size();i++){
                List<Double> outputPair= new ArrayList<>();
                outputPair.add(pair.getIdeal().getData(i));
                outputPair.add(output.getData(i));
                testingResults.add(outputPair);
                System.out.println("actual= "+output.getData(i)+",ideal="+pair.getIdeal().getData(i));
            }
        }
        return testingResults;
    }
    
    public List<List<Double>> testNetwork(){
        testingResults = new ArrayList<>();
        for(MLDataPair pair : testingDataSet){
            final MLData output = network.compute(pair.getInput());
            for(int i=0; i<output.size();i++){
                List<Double> outputPair= new ArrayList<>();
                outputPair.add(pair.getIdeal().getData(i));
                outputPair.add(output.getData(i));
                testingResults.add(outputPair);
                System.out.println("actual= "+output.getData(i)+",ideal="+pair.getIdeal().getData(i));
            }
        }
        return testingResults;
    }
    public void persistNetwork(String filePath){
        EncogDirectoryPersistence.saveObject(new File(filePath), network);
    }
    
    public void shutDownEngine(){
        Encog.getInstance().shutdown();
    }
}
