package com.training.machinelearningjavaencog.neusysops.data.management;

import java.util.List;
import org.apache.commons.math3.util.DoubleArray;
/**
 *
 * @author rorodriguez
 */
public class DataCasting {
    public static double[] castFromStringListToDoubleArray(List<String> values){
        double[] castedValues= new double[values.size()];
        for(int index=0;index<values.size();index++){
            castedValues[index]= Double.parseDouble(values.get(index));
        }
        return castedValues;
    }
    
    public static double[][] castFromStringListToDoubleMatrix(List<List<String>> values){
        double[][] castedValues= new double[values.size()][values.get(0).size()];
        for(int i= 0;i<castedValues.length;i++){
            for(int j=0;j<castedValues[i].length;j++){
                castedValues[i][j]= Double.parseDouble(values.get(i).get(j));
            }
        }
        return castedValues;
    }
}
