package com.training.machinelearningjavaencog.neusysops.data.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;



/**
 *
 * @author rorodriguez
 */
public class FileLoading {
    public static File loadFile(String rutaArchivo){
        File archivo = new File(rutaArchivo);
        return archivo;
    }
}
