//NeuroEvolution of Augmenting Topologies -- Clasification Problems?
package com.training.machinelearningjavaencog.neusysops.network;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationStep;
import org.encog.ml.CalculateScore;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.ml.ea.train.EvolutionaryAlgorithm;
import org.encog.ml.train.MLTrain;
import org.encog.neural.neat.NEATNetwork;
import org.encog.neural.neat.NEATPopulation;

import org.encog.neural.neat.NEATUtil;
import org.encog.neural.networks.training.TrainingSetScore;
import org.encog.persist.EncogDirectoryPersistence;
import org.encog.util.simple.EncogUtility;
/**
 *
 * @author rorodriguez
 */
public class NEAT {
    NEATNetwork network;
    NEATPopulation pop;
    MLDataSet trainingDataSet;
    MLDataSet testingDataSet;
    TrainingSetScore score;
    ActivationStep step;
    EvolutionaryAlgorithm evolutionaryAlgorithmTrain;
    List<List<Double>> testingResults;
    
    public NEAT(int numberOfInputNeurons, int numberOfOutputNeurons, int populationSize){
        this.pop = new NEATPopulation(numberOfInputNeurons, numberOfOutputNeurons, populationSize);
    }
    
    public void setTrainingDataSet(double[][] input, double[][] ideal){
        trainingDataSet= new BasicMLDataSet(input,ideal);
    }
    
    public void setTestingDataSet(double[][] input, double[][] ideal){
        testingDataSet= new BasicMLDataSet(input,ideal);
    }
    
    public void setInitialConnectionDensity(double connectionDensity){
        pop.setInitialConnectionDensity(connectionDensity);
    }
    
    public void resetNetworkPopulation(){
        pop.reset();
    }
    
    public void calculateTrainingScore(){
        score = new TrainingSetScore(trainingDataSet);
    }
    
    public void setActivationStep(){
        step = new ActivationStep();
        step.setCenter(0.5);
        pop.setNEATActivationFunction(step);
    }
    
    public void constructNetworkTrainerWithNEATUtil(){
        evolutionaryAlgorithmTrain = NEATUtil.constructNEATTrainer(pop,score);
    }
    
    public void trainPopulationWithEvolutionaryAlgorithm(double error){
        do {
		evolutionaryAlgorithmTrain.iteration();
		System.out.println("Epoch #" + evolutionaryAlgorithmTrain.getIteration() + " Error:" + evolutionaryAlgorithmTrain.getError()+ ", Species:" + pop.getSpecies().size());
	} while(evolutionaryAlgorithmTrain.getError() > error);
    }
    
    public void trainPopulationWithEncogUtility(){
        EncogUtility.trainToError((MLTrain) evolutionaryAlgorithmTrain, 0.01);
    }
    
    public void extractBestSpecimenFromPopulation(){
        network = (NEATNetwork)evolutionaryAlgorithmTrain.getCODEC().decode(evolutionaryAlgorithmTrain.getBestGenome());
    }
    
    public List<List<Double>> testNetwork(){
        testingResults = new ArrayList<>();
        System.out.println("Neural Network Results:");
        for(MLDataPair pair : testingDataSet){
            final MLData output = network.compute(pair.getInput());
            for(int i=0; i<output.size();i++){
                List<Double> outputPair= new ArrayList<>();
                outputPair.add(pair.getIdeal().getData(i));
                outputPair.add(output.getData(i));
                testingResults.add(outputPair);
                System.out.println("actual= "+output.getData(i)+",ideal="+pair.getIdeal().getData(i));
            }
        }
        return testingResults;
    }
    
    public void testNetworkWithEncogUtility(){
        System.out.println("Neural Network Results:");
        EncogUtility.evaluate(network, testingDataSet);
    }
    
    public void persistNetwork(String filePath){
        EncogDirectoryPersistence.saveObject(new File(filePath), network);
    }
    
    public void shutDownEngine(){
        Encog.getInstance().shutdown();
    }
}
