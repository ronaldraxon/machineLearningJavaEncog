package com.training.machinelearningjavaencog.neusysops.data.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author rorodriguez
 */
public class FileInteraction {
    public static List<String> extractObservationsFromTxtResource(String filePath) throws FileNotFoundException, IOException{
        FileInteraction fi = new FileInteraction();
        File file = fi.getResourceFile(filePath);
        List observaciones;
        try (FileReader lectorArchivo = new FileReader(file)) {
            BufferedReader br = new BufferedReader(lectorArchivo);
            observaciones = new ArrayList();
            String observacion;
            while((observacion=br.readLine())!=null){
                observaciones.add(observacion);
            }   br.close();
        }
        return observaciones;
    }
    
    public static List<List<String>> extractObservationsFromCSVResource(String filePath) throws FileNotFoundException, IOException {
        FileInteraction fi = new FileInteraction();
        File file = fi.getResourceFile(filePath);
        List<List<String>> observaciones;
        try (FileReader fileReader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(fileReader);
            observaciones = new ArrayList();
            String linea;
            while((linea=br.readLine())!=null){
                String[] arregloObservacion = linea.split(",");
                List<String> observacion = new ArrayList();
                for(String valor : arregloObservacion){
                    observacion.add(valor);
                }
                observaciones.add(observacion);
            }   br.close();
        }
        return observaciones;
    }
    
    public static void writeObservationsMatrixToCSVResource(String filePath,List<List<String>> observations) throws FileNotFoundException, IOException{
        FileInteraction fi = new FileInteraction();
        File file = fi.getResourceFile(filePath);
        try (PrintWriter pw = new PrintWriter(file)) {
            observations.forEach((observation)-> {
                pw.append(Arrays.toString(observation.toArray()).replace("[", "").replace("]", "").replace(" ", ""));
                pw.append('\n');
                }
            );
        }
    }
    
    public static void writeDoubleObservationsMatrixToCSVResource(String filePath,List<List<Double>> observations) throws FileNotFoundException, IOException{
        FileInteraction fi = new FileInteraction();
        File file = new File(fi.getResourcePath(filePath));
        try (PrintWriter pw = new PrintWriter(file)) {
            observations.forEach((observation)-> {
                pw.append(Arrays.toString(observation.toArray()).replace("[", "").replace("]", "").replace(" ", ""));
                pw.append('\n');
                }
            );
        }
    }
    
    private File getResourceFile(String filePath) throws IOException{
        return new File(getClass().getResource(filePath).getFile());
    }
    
    private String getResourcePath(String filePath) throws IOException{
        return getClass().getResource(filePath).getPath();
    }
    
    public static List<String> extractObservationsFromTxtFile(String filePath) throws FileNotFoundException, IOException{
        File file = new File(filePath);
        FileReader lectorArchivo = new FileReader(file);
        BufferedReader br = new BufferedReader(lectorArchivo);
        List observaciones = new ArrayList();
        String observacion;
        while((observacion=br.readLine())!=null){
            observaciones.add(observacion);
        }
        return observaciones;
    }
    
    public static List<List<String>> extractObservationsFromCSVFile(String filePath) throws FileNotFoundException, IOException {
        File archivo = new File(filePath);
        FileReader lectorArchivo = new FileReader(archivo);
        BufferedReader br = new BufferedReader(lectorArchivo);
        List<List<String>> observaciones = new ArrayList();
        String linea;
        while((linea=br.readLine())!=null){
            String[] arregloObservacion = linea.split(";");
            List<String> observacion = new ArrayList();
            for(String valor : arregloObservacion){
                observacion.add(valor);
            }
            observaciones.add(observacion);
        }
        br.close();
        lectorArchivo.close();
        return observaciones;
    }
    
    public static void writeObservationsMatrixToCSV(String rutaArchivo,List<List<String>> observations) throws FileNotFoundException, IOException{
        File archivo = new File(rutaArchivo);
        try (PrintWriter pw = new PrintWriter(archivo)) {
            observations.forEach((observacion)-> {
                pw.append(Arrays.toString(observacion.toArray()).replace("[", "").replace("]", "").replace(" ", ""));
                pw.append('\n');
                }
            );
        }
    }
    
    public static void writeDoubleObservationsMatrixToCSV(String rutaArchivo,List<List<Double>> observations) throws FileNotFoundException, IOException{
        File archivo = new File(rutaArchivo);
        try (PrintWriter pw = new PrintWriter(archivo)) {
            observations.forEach((observacion)-> {
                pw.append(Arrays.toString(observacion.toArray()).replace("[", "").replace("]", "").replace(" ", ""));
                pw.append('\n');
                }
            );
        }
    }
}
