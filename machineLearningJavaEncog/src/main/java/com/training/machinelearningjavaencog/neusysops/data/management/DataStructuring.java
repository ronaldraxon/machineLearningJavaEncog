
package com.training.machinelearningjavaencog.neusysops.data.management;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

/**
 *
 * @author rorodriguez
 */
public class DataStructuring {
    public static double[][] toNColumnArrayStructure(double[] values, int numberOfColumns){    
        int numberOfRows = values.length/numberOfColumns;
        double[][] observations = new double[numberOfRows][numberOfColumns];
        List<Double> doubleList = Arrays.stream(values).boxed().collect(Collectors.toList());
        List<Double> observationValues;
        int index=0;
        for(int i=0;i<numberOfRows;i++){
            observationValues = doubleList.subList(index, index+numberOfColumns);
            double[] array = new double[numberOfColumns];
            for(int j=0;j<numberOfColumns;j++){
                array[j] = observationValues.get(j);
            }
            observations[i] = array;
            index=index+numberOfColumns;
        }
        return observations;
    }
    
    public static double[][] toNColumnArrayStructureWithLag(double[] values, int numberOfColumns,int lag){    
        int numberOfRows = values.length-numberOfColumns;
        if(numberOfRows==0){
            numberOfRows =1;
        }
        double[][] observations = new double[numberOfRows][numberOfColumns];
        List<Double> doubleList = Arrays.stream(values).boxed().collect(Collectors.toList());
        List<Double> observationValues;
        int index=0;
        for(int i=0;i<numberOfRows;i++){
            observationValues = doubleList.subList(index, index+numberOfColumns);
            double[] array = new double[numberOfColumns];
            for(int j=0;j<numberOfColumns;j++){
                array[j] = observationValues.get(j);
            }
            observations[i] = array;
            index=index+lag;
        }
        return observations;
    }
    
    public static double[] toSingleColumnArrayStructure(double[][] values){    
        int numberOfColumns = values[0].length;
        int numberOfRecords = values.length * numberOfColumns;
        double[] observations = new double[numberOfRecords];
        int index=0;
        for (double[] value : values) {
            for (int j = 0; j<numberOfColumns; j++) {
                observations[index] = value[j];
                index++;
            }
        }
        return observations;
    }
    
    public static List<List<String>> timeSerieSetWithExogenousVariables(List<List<String>> values){
        List<List<String>> observations = new ArrayList<>();
        for(int i =0; i<values.size()-1;i++){
            List<String> observation = new ArrayList<>();
            observation.addAll(values.get(i).subList(0, values.get(i).size()));
            observation.add(values.get(i+1).get(values.get(i+1).size()-1));
            observations.add(observation);
        }
        return observations;
    }
    
    public static List<List<String>> timeSerieSetWithWindowAndLag(List<String> values, Integer window, Integer lag){
        List<List<String>> observations = new ArrayList<>();
        for(int i =0; i<values.size();i=i+lag){
            List<String> observation = new ArrayList<>();
            observation.addAll(values.subList(i, window));
            observations.add(observation);
        }
        return observations;
    }
}
