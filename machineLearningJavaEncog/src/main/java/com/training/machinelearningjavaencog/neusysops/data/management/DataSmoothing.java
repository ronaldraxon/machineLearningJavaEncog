/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.training.machinelearningjavaencog.neusysops.data.management;

/**
 *
 * @author rorodriguez
 */
public class DataSmoothing {
    
    public static double returnValueFromExponentialSmoothing(double smoothedValue,double alfa, double previousX,double powNumber){
        return (smoothedValue-(alfa*Math.pow(1-alfa, powNumber)*previousX))/alfa;
    }
    
    public static double smoothValueWithExponentialSmoothing(double valueX,double alfa, double previousX, double powNumber){
        return (alfa*valueX)+(alfa*(1-alfa)*previousX);
    }
}
