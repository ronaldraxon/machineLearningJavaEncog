package com.training.machinelearningjavaencog.neusysops.data.management;

import org.apache.commons.math3.stat.StatUtils;

/**
 *
 * @author rorodriguez
 */
public class DataStatistics {
    public static double getMaxValueFromArray(double[] values){
        return StatUtils.max(values);
    }
    public static double getMinValueFromArray(double[] values){
        return StatUtils.min(values);
    }
}
