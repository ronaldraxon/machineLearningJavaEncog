package com.training.machinelearningjavaencog.neusysops.data.management;

import org.apache.commons.math3.stat.StatUtils;
/**
 *
 * @author rorodriguez
 */
public class DataScaling {
    public static double[] standardScoreScaling(double[] values){
        return StatUtils.normalize(values);
    }
    
    public static double[] arrayFeatureScaling(double[] values){
        double maxValue = DataStatistics.getMaxValueFromArray(values);
        double minValue = DataStatistics.getMinValueFromArray(values);
        double[] scaledValues = new double[values.length];
        for(int index=0;index<values.length;index++){
            scaledValues[index]= valueFeatureScaling(values[index],minValue,maxValue);
        }
        return scaledValues;
    }
    
    public static double valueFeatureScaling(double value,double minValue, double maxValue){
        return (value-minValue)/(maxValue-minValue);
    }
    
    public static double[] arrayFeatureScalingToNormalValues(double[] values,double minValue, double maxValue){
        double[] scaledValues = new double[values.length];
        for(int index=0;index<values.length;index++){
            scaledValues[index]= valueFeatureScalingToNormalValue(values[index],minValue,maxValue);
        }
        return scaledValues;
    }
    
    public static double valueFeatureScalingToNormalValue(double value,double minValue, double maxValue){
        return (value*(maxValue-minValue))+minValue ;
    }
    
    public static double[][] matrixFeatureScaling(double[][] values, double[][] minMaxMatrix){
        double[][] scaledValues= new double[values.length][values[0].length];
        for(int i = 0;i<scaledValues.length;i++){
            for(int j =0;j<scaledValues[i].length;j++){
                scaledValues[i][j] = valueFeatureScaling(values[i][j],minMaxMatrix[0][j],minMaxMatrix[1][j]);
            }
        }
        return scaledValues;
    }
    
}
