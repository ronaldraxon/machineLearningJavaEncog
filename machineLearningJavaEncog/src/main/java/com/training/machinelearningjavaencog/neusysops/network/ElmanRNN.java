package com.training.machinelearningjavaencog.neusysops.network;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationLinear;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.engine.network.activation.ActivationTANH;
import org.encog.ml.CalculateScore;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.ml.train.MLTrain;
import org.encog.ml.train.strategy.Greedy;
import org.encog.ml.train.strategy.HybridStrategy;
import org.encog.ml.train.strategy.StopTrainingStrategy;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.TrainingSetScore;
import org.encog.neural.networks.training.anneal.NeuralSimulatedAnnealing;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.neural.pattern.ElmanPattern;
import org.encog.neural.pattern.JordanPattern;
import org.encog.persist.EncogDirectoryPersistence;
    

public class ElmanRNN {
    BasicNetwork network;
    MLDataSet trainingDataSet;
    MLDataSet testingDataSet;
    List<List<Double>> testingResults;
    ElmanPattern pattern;
    MLTrain trainAlt;
    MLTrain trainMain;
    
    public ElmanRNN(){
        pattern = new ElmanPattern();
    }
    
    public void setTrainingDataSet(double[][] input, double[][] ideal){
        trainingDataSet= new BasicMLDataSet(input,ideal);
    }
    
    public void setTestingDataSet(double[][] input, double[][] ideal){
        testingDataSet= new BasicMLDataSet(input,ideal);
    }
    
    public void setLinearActivationToNetwork(){
        pattern.setActivationFunction(new ActivationLinear());
    }
    
    public void setSigmoideanActivationToNetwork(){
        pattern.setActivationFunction(new ActivationSigmoid());
    }
    
    public void setInputNeurons(int numberOfNeurons){
        pattern.setInputNeurons(numberOfNeurons);
    }
    
    public void setHiddenNeurons(int numberOfNeurons){
        pattern.addHiddenLayer(numberOfNeurons);
    }
    
    public void setOutputNeurons(int numberOfNeurons){
        pattern.setOutputNeurons(numberOfNeurons);
    }

    public void generatePatternIntoNeuralNetwork(){
        network = (BasicNetwork)pattern.generate();
    }
    
    public void addBasicLayerToNetwork(Boolean hasBiasNeuron, Integer numberOfNeurons){
        network.addLayer(new BasicLayer(null,hasBiasNeuron,numberOfNeurons));
    }
    
    public void addBasicLinearLayerToNetwork(Boolean hasBiasNeuron, Integer numberOfNeurons){
        network.addLayer(new BasicLayer(new ActivationLinear(),hasBiasNeuron,numberOfNeurons));
    }
    
    public void addBasicSigmoideanLayerToNetwork(Boolean hasBiasNeuron, Integer numberOfNeurons){
        network.addLayer(new BasicLayer(new ActivationSigmoid(),hasBiasNeuron,numberOfNeurons));
    }
    
    public void addBasicTanHLayerToNetwork(Boolean hasBiasNeuron, Integer numberOfNeurons){
        network.addLayer(new BasicLayer(new ActivationTANH(),hasBiasNeuron,numberOfNeurons));
    }
    
    public void trainNetwork(double expectedError){
        //train the neural network
        CalculateScore score = new TrainingSetScore(trainingDataSet);
        trainAlt = new NeuralSimulatedAnnealing(network, score, 200, 2, 100);
	//trainMain = new Backpropagation(network, trainingDataSet,0.01, 0.6);
        trainMain = new Backpropagation(network, trainingDataSet);
	StopTrainingStrategy stop = new StopTrainingStrategy();
	trainMain.addStrategy(new Greedy());
        trainMain.addStrategy(new HybridStrategy(trainAlt));
	trainMain.addStrategy(stop);
	int epoch = 0;
            //while (!stop.shouldStop() || trainMain.getError() > expectedError) {
            while (!stop.shouldStop()) {    
                trainMain.iteration();
		System.out.println("Epoch #" + epoch + " Error:" + trainMain.getError());
		epoch++;
            }
        
    }
    
    public List<List<Double>> testNetwork(){
        testingResults = new ArrayList<>();
        for(MLDataPair pair : testingDataSet){
            final MLData output = network.compute(pair.getInput());
            for(int i=0; i<output.size();i++){
                List<Double> outputPair= new ArrayList<>();
                outputPair.add(pair.getIdeal().getData(i));
                outputPair.add(output.getData(i));
                testingResults.add(outputPair);
                System.out.println("actual= "+output.getData(i)+",ideal="+pair.getIdeal().getData(i));
            }
        }
        return testingResults;
    }
    
    public void persistNetwork(String filePath){
        EncogDirectoryPersistence.saveObject(new File(filePath), network);
    }
    
    public void shutDownEngine(){
        Encog.getInstance().shutdown();
    }
}
